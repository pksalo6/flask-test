from flask import Flask, render_template
from flask import request

app = Flask(__name__, template_folder='templates')

@app.route('/', methods=['GET'])
def home():
	file_name = request.args.get('file_name', default = 'file1.txt')
	start_line = request.args.get('start_line', type = int)
	end_line = request.args.get('end_line', type = int)

	message = ''
	
	if start_line and end_line:
		if start_line >= end_line:
			message = 'Start line must be smaller than end line'
			return render_template('index.html', message = message)
		if start_line < 1 or end_line < 1:
			message = 'Start line and end line must be positive integer.'
			return render_template('index.html', message = message)

	fetched_lines= []
	try:
		file_ptr = open('files/'+file_name)
		lines = file_ptr.readlines()

		if start_line and end_line:
			fetched_lines = lines[start_line-1:end_line]
		else:
			fetched_lines = lines[:]

		message = 'File '+file_name+' is get processed properly.'

	except FileNotFoundError as e:
		message = 'File '+ file_name+' not exist to process.'

	return render_template('index.html', message = message, fetched_lines=fetched_lines)

if __name__ == '__main__':
	app.run()